\documentclass[11pt, a4paper, twocolumn]{article}
\usepackage[top = 1cm, bottom = 2cm, left = 1cm, right = 1cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref, float}
\usepackage[inkscapelatex = false]{svg}

\title{
  Solvers for coupled sparse/dense FEM/BEM linear systems
}
\author{Marek \textsc{Felšöci}}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}

This is an example experimental study relying on the \texttt{test\_FEMBEM}
\cite{testFEMBEM} solver test suite. Here, we are especially interested in
solving coupled sparse/dense FEM/BEM linear systems arising in the domain of
aeroacoustics. The idea is to evaluate the solvers available in the open-source
version of \texttt{test\_FEMBEM} for the solution of this kind of linear
systems.

\section{Experimental study}

Unfortunately, the open-source version of \texttt{test\_FEMBEM} does not
implement couplings of sparse and dense direct solvers which is normally our
go-to method for solving sparse/dense FEM/BEM systems. Therefore, we rely here
only on dense direct solvers, namely HMAT-OSS and Chameleon.

HMAT-OSS \cite{hmat-oss} is an open-source and sequential version of the
compressed hierarchical \(\mathcal{H}\)-Matrix dense direct solver HMAT
\cite{Lize14} developed at Airbus. Chameleon \cite{chameleon} is a fully
open-source dense direct solver without compression.

HMAT-OSS and Chameleon rely on BLAS \cite{BLAS} and LAPACK \cite{LAPACK}
routines for various matrix operations. These routines have multiple
implementations. In this study, we use and compare two different
implementations, the open-source OpenBLAS \cite{OpenBLAS} implementation and the
non-free vendor-specific Intel(R) Math Kernel Library (MKL) \cite{MKL}.

As of the test case, we consider a simplified \textit{short pipe} which is still
close enough to real-life models (see Figure \ref{short-pipe}).

Note that all the benchmarks were conducted on a single dodeca-core (16 threads)
Intel(R) Core(TM) i5-12500H (generation 12) clocked at 4.5GHz \cite{cpu} machine
with Hyper-Threading enabled and 16 GB of RAM.

\begin{figure}[H]
  \centering
  \includegraphics[width = .5\columnwidth]{figures/short-pipe}
  \caption{A \textit{short pipe} mesh counting 20,000 vertices.}
  \label{short-pipe}
\end{figure}

\subsection{Data compression}

In the first part, we want to know to which extent can data compression improve
the computation time. For this, we compare sequential executions of both
HMAT-OSS, the compressed solver, and Chameleon, the non-compressed solver, on
coupled FEM/BEM systems of different sizes (see Figure \ref{hmat-chameleon}).
The solvers were compiled and run either with OpenBLAS or Intel(R) MKL.

The results clearly show the advantage of using data compression, especially
with increasing size of the target linear system.

\begin{figure}
  \centering
  \includesvg[width = 1\columnwidth]{figures/hmat-chameleon}
  \caption{ Computation times of sequential runs of HMAT-OSS and Chameleon on
    coupled sparse/dense FEM/BEM linear systems of varying size. The solvers
    were compiled either with OpenBLAS or Intel(R) MKL. }
  \label{hmat-chameleon}
\end{figure}

For these experiments, we have considered the precision parameter $\epsilon$ for
the HMAT-OSS solver to be $10^{-3}$. In Figure \ref{hmat-chameleon-error}, the
relative error curve for the runs presented in Figure \ref{hmat-chameleon}
verifies that the threshold is respected and that the error of the solutions
computed by HMAT-OSS is even smaller than $\epsilon$.

\begin{figure}
  \centering
  \includesvg[width = 1\columnwidth]{figures/hmat-chameleon-error}
  \caption{ Relative error of sequential runs of HMAT-OSS and Chameleon on
    coupled sparse/dense FEM/BEM linear systems of varying size. The solvers
    were compiled either with OpenBLAS or Intel(R) MKL. }
  \label{hmat-chameleon-error}
\end{figure}

The differences between the runs with OpenBLAS and the runs with Intel(R) MKL
are very small in terms of both computation times and relative error.

\subsection{Multi-threaded execution}

To study the impact of parallel execution on the time to solution, we limit
ourselves to the Chameleon solver as HMAT-OSS is sequential-only. In Figure
\ref{chameleon}, we comapre the computation times of Chameleon on coupled
FEM/BEM systems of different sizes using either one or four threads. The solver
were compiled and run either with OpenBLAS or Intel(R) MKL.

According to the results, we can observe a significant decrease in computation
time in case of parallel executions. For the sequential runs, the differences
between using OpenBLAS and MKL are again barely noticeables. However, in
parallel and on a larger test case (8000 unknowns), the vendor-specific
implementation of BLAS, i.e. Intel(R) MKL, delivers better performance than its
open-source counterpart OpenBLAS.

\begin{figure}
  \centering
  \includesvg[width = 1\columnwidth]{figures/chameleon}
  \caption{ Computation times of sequential and parallel runs of Chameleon on
    coupled sparse/dense FEM/BEM linear systems of varying size. The solvers
    were compiled either with OpenBLAS or Intel(R) MKL. }
  \label{chameleon}
\end{figure}

\section{Conclusion}

We have evaluated the performance of the solvers linked to the
\texttt{test\_FEMBEM} test suite on coupled sparse/dense FEM/BEM linear systems.
The solvers considered were HMAT-OSS, a sequential compressed dense direct
solver and Chameleon, a multi-threaded non-compressed dense direct solver.
Moreover, we have compiled and compared the solvers on two different
implementations of BLAS and LAPACK routines, the open-source OpenBLAS and the
non-free vendor-specific Intel(R) MKL.

The comparison of sequential runs of HMAT-OSS and Chameleon showed an important
positive impact of data compression on the time to solution with only a very
small difference between the BLAS and LAPACK implementations. In addition, the
comparison of sequential and parallel runs of Chameleon as well as the computed
parallel efficiency showed a considerable speed-up of the parallel execution. In
this case, the vendor-specific Intel(R) MKL library seems to deliver better
performance. This was especially noticeable on a larger test case.

\bibliography{references}
\bibliographystyle{siam}

\end{document}
