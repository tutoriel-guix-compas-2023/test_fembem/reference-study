FROM debian:bullseye
LABEL author="Marek Felšöci <marek.felsoci@inria.fr>"

# Use Bash shell instead of the default 'sh' shell.
SHELL [ "/bin/bash", "-c" ]
ENV SHELL=/bin/bash

# Force non-interactive Debian front-end.
ENV DEBIAN_FRONTEND="noninteractive"
# Set the locale and the time zone.
ENV LANG="sk_SK.UTF-8" LANGUAGE="sk" TZ="Europe/Bratislava"
# Define the location of source files and the location of built binaries.
ENV SOURCES=/src BINS=/bin
# Add '/usr/local/lib' to the library path.
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
# Use sequential BLAS by default.
ENV OMP_NUM_THREADS=1

# Update software sources.
RUN apt update
# Install locales.
RUN apt install -y locales-all
# Install additional packages allowing us to register Intel(R) repositories.
RUN apt install -y wget gpg ca-certificates
# Register Intel(R) repositories.
RUN wget -O- \
    https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB \
    | gpg --dearmor | \
    tee /usr/share/keyrings/oneapi-archive-keyring.gpg > /dev/null
RUN echo "deb [signed-by=/usr/share/keyrings/oneapi-archive-keyring.gpg]" \
    "https://apt.repos.intel.com/oneapi all main" | \
    tee /etc/apt/sources.list.d/oneAPI.list
# Update software sources again.
RUN apt update
# Install pre-built dependencies.
RUN apt install -y git build-essential pkg-config python3 python3-dev \
    python3-pygments flex bison r-base r-base-dev r-cran-ggplot2 \
    r-cran-svglite inkscape texlive-full latexmk libhwloc15 libhwloc-dev cmake \
    openmpi-bin openmpi-common openmpi-doc libopenmpi3 libopenmpi-dev \
    intel-oneapi-mkl intel-oneapi-mkl-devel
# Create a directory to store source files in and set it as current working
# directory.
WORKDIR ${SOURCES}
# Download and extract source files of:
# - 'jemalloc',
RUN wget https://github.com/jemalloc/jemalloc/releases/download/4.5.0/jemalloc-4.5.0.tar.bz2
RUN mkdir -p ${SOURCES}/jemalloc
RUN tar -xvf jemalloc-4.5.0.tar.bz2 -C jemalloc --strip-components 1
# - 'StarPU',
RUN wget https://files.inria.fr/starpu/starpu-1.3.9/starpu-1.3.9.tar.gz
RUN mkdir -p ${SOURCES}/starpu
RUN tar -xvzf starpu-1.3.9.tar.gz -C starpu --strip-components 1
# - 'OpenBLAS',
RUN git clone https://github.com/xianyi/OpenBLAS.git
WORKDIR ${SOURCES}/OpenBLAS
RUN git checkout v0.3.20 
WORKDIR ${SOURCES}
# - 'Chameleon',
RUN wget https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz
RUN mkdir -p ${SOURCES}/openblas/chameleon/build
RUN tar -xvf chameleon-1.1.0.tar.gz -C openblas/chameleon --strip-components 1
RUN mkdir -p ${SOURCES}/mkl/chameleon/build
RUN tar -xvf chameleon-1.1.0.tar.gz -C mkl/chameleon --strip-components 1
# - 'HMAT' (sequential open-source edition),
RUN git clone https://github.com/jeromerobert/hmat-oss
WORKDIR ${SOURCES}/hmat-oss
RUN git checkout 6ad59b7db2be69ac3f60e4a58795b71855e1b523
WORKDIR ${SOURCES}
RUN mkdir -p hmat-oss/build
RUN cp -ar hmat-oss openblas/hmat-oss
RUN mv hmat-oss mkl/hmat-oss
# - 'test_FEMBEM'.
RUN git clone https://gitlab.inria.fr/solverstack/test_fembem
WORKDIR ${SOURCES}/test_fembem
RUN git checkout 59d1983493a95ec483b4d931afcc70b687474aa5
WORKDIR ${SOURCES}
RUN mkdir -p test_fembem/build
RUN cp -ar test_fembem openblas/test_fembem
RUN mv test_fembem mkl/test_fembem
# Build and install 'OpenBLAS'.
WORKDIR ${SOURCES}/OpenBLAS
RUN make -j$(nproc)
RUN make PREFIX=${BINS}/OpenBLAS/ install
# Build and install 'jemalloc'.
WORKDIR ${SOURCES}/jemalloc
RUN ./configure --prefix=${BINS}/jemalloc
RUN make -j$(nproc) install
# Build and install 'StarPU'.
WORKDIR ${SOURCES}/starpu
RUN ./configure --enable-maxcpus=128 --disable-build-examples \
                --disable-build-doc --prefix=${BINS}/starpu
RUN make -j$(nproc) install
# Build and install 'Chameleon' (using OpenBLAS).
WORKDIR ${SOURCES}/openblas/chameleon/build
RUN cmake -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=OpenBLAS \
          -DBUILD_SHARED_LIBS=ON \
          -DCMAKE_PREFIX_PATH="${BINS}/OpenBLAS;${BINS}/starpu" \
          -DCMAKE_INSTALL_PREFIX=${BINS}/openblas/chameleon ..
RUN make -j$(nproc) install
# Build and install 'HMAT' (using OpenBLAS).
WORKDIR ${SOURCES}/openblas/hmat-oss/build
RUN cmake -DHMAT_JEMALLOC=ON -DHMAT_EXPORT_BUILD_DATE=ON \
          -DCMAKE_INSTALL_PREFIX=${BINS}/openblas/hmat-oss \
          -DCMAKE_PREFIX_PATH="${BINS}/jemalloc;${BINS}/starpu;${BINS}/OpenBLAS" ..
RUN make -j$(nproc) install
# Build and install 'test_FEMBEM' (using OpenBLAS).
WORKDIR ${SOURCES}/openblas/test_fembem/build
RUN cmake -DCMAKE_INSTALL_PREFIX=${BINS}/openblas/test_fembem \
          -DCMAKE_PREFIX_PATH="${BINS}/starpu;${BINS}/OpenBLAS;${BINS}/openblas/chameleon;${BINS}/openblas/hmat-oss" \
          ..
RUN LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BINS}/jemalloc/lib:${BINS}/starpu/lib:${BINS}/openblas/chameleon/lib:${BINS}/openblas/hmat-oss/lib" \
    make -j$(nproc) install
# Turn on Intel(R) software environment.
ARG TBBROOT=/opt/intel/oneapi/tbb/latest/env/..
ARG ONEAPI_ROOT=/opt/intel/oneapi
ARG PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/opt/intel/oneapi/tbb/latest/env/../lib/pkgconfig:/opt/intel/oneapi/mkl/latest/lib/pkgconfig:/opt/intel/oneapi/compiler/latest/lib/pkgconfig
ARG DIAGUTIL_PATH=/opt/intel/oneapi/compiler/latest/sys_check/sys_check.sh
ARG MANPATH=$MANPATH:/opt/intel/oneapi/compiler/latest/documentation/en/man/common:
ARG CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}:/opt/intel/oneapi/tbb/latest/env/..:/opt/intel/oneapi/compiler/latest/linux/IntelDPCPP
ARG CMPLR_ROOT=${CMPLR_ROOT}:/opt/intel/oneapi/compiler/latest
ARG LIBRARY_PATH=${LIBRARY_PATH}:/opt/intel/oneapi/tbb/latest/env/../lib/intel64/gcc4.8:/opt/intel/oneapi/mkl/latest/lib/intel64:/opt/intel/oneapi/compiler/latest/linux/compiler/lib/intel64_lin:/opt/intel/oneapi/compiler/latest/linux/lib
ARG OCL_ICD_FILENAMES=${OCL_ICD_FILENAMES}:/opt/intel/oneapi/compiler/latest/linux/lib/x64/libintelocl.so
ARG LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/intel/oneapi/tbb/latest/env/../lib/intel64/gcc4.8:/opt/intel/oneapi/mkl/latest/lib/intel64:/opt/intel/oneapi/compiler/latest/linux/compiler/lib/intel64_lin:/opt/intel/oneapi/compiler/latest/linux/lib:/usr/local/lib
ARG MKLROOT=/opt/intel/oneapi/mkl/latest
ARG PATH=${PATH}:/opt/intel/oneapi/mkl/latest/bin/intel64:/opt/intel/oneapi/compiler/latest/linux/bin/intel64:/opt/intel/oneapi/compiler/latest/linux/bin
ARG CPATH=${CPATH}:/opt/intel/oneapi/tbb/latest/env/../include:/opt/intel/oneapi/mkl/latest/include
# Build and install 'Chameleon' (using MKL).
WORKDIR ${SOURCES}/mkl/chameleon/build
RUN cmake -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=Intel10_64lp \
          -DBUILD_SHARED_LIBS=ON -DCMAKE_PREFIX_PATH="${BINS}/starpu" \
          -DCMAKE_INSTALL_PREFIX=${BINS}/mkl/chameleon ..
RUN make -j$(nproc) install
# Build and install 'HMAT' (using MKL).
WORKDIR ${SOURCES}/mkl/hmat-oss/build
RUN cmake -DHMAT_JEMALLOC=ON -DHMAT_EXPORT_BUILD_DATE=ON \
          -DCMAKE_INSTALL_PREFIX=${BINS}/mkl/hmat-oss \
          -DCMAKE_PREFIX_PATH="${BINS}/jemalloc;${BINS}/starpu;" ..
RUN make -j$(nproc) install
# Build and install 'test_FEMBEM' (using MKL).
WORKDIR ${SOURCES}/mkl/test_fembem/build
RUN cmake -DCMAKE_INSTALL_PREFIX=${BINS}/mkl/test_fembem \
          -DCMAKE_PREFIX_PATH="${BINS}/starpu;${BINS}/mkl/chameleon;${BINS}/mkl/hmat-oss" \
          ..
RUN LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${LIBRARY_PATH}:${BINS}/jemalloc/lib:${BINS}/starpu/lib:${BINS}/openblas/mkl/lib:${BINS}/mkl/hmat-oss/lib" \
    make -j$(nproc) install