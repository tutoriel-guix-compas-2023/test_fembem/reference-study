# test_FEMBEM: Reference study

[![pipeline
status](https://gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/reference-study/badges/master/pipeline.svg)](https://gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/reference-study/-/commits/master)

An experimental study relying on the open-source edition of the `test_FEMBEM`
solver [3] linked to the Chameleon [4] and HMAT-OSS [5] matrix libraries.

[View
PDF](https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/test_fembem/reference-study/study.pdf)

## Reproducing guidelines

The benchmarks the study is based on were carried on a laptop equipped with an
dodeca-core (16 threads) Intel(R) Core(TM) i5-12500H processor of 12th
generation clocked at 4.5GHz (Hyper-Threading enabled) [10], 16 GB of RAM and
running *Debian GNU/Linux 11*.

This section further provides instructions for building the required software
environment, running the experiments, post-processing the results and producing
the final PDF document.

### Software environment

For an easy access to the software environment we used for running experiments,
post-processing results and producing the PDF of the study, we provide a
pre-built Docker [1] software container.

To acquire the Docker container, use the following command.

```shell
docker pull registry.gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/reference-study:latest
```

Note that the `Dockerfile`, i.e. the recipe, used to build the container is
present in the root of this repository. See [2] for further information on
building Docker containers.

#### Manual build

In addition to the Docker container and the corresponding build recipe, we
provide instructions for a manual build of the software environment assuming a
personal computer with superuser privileges and running *Debian GNU/Linux 11*.

Most of the software packages can be installed from Debian software
repositories. However, to install the Intel(R) Math Kernel Library (MKL) [7], we
need to add an extra package source to our APT configuration:

```shell
sudo apt update
wget -O- \
  https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB \
  | gpg --dearmor | \
  sudo tee /usr/share/keyrings/oneapi-archive-keyring.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/oneapi-archive-keyring.gpg]" \
     "https://apt.repos.intel.com/oneapi all main" | \
     sudo tee /etc/apt/sources.list.d/oneAPI.list
sudo apt update
```

Then, we can install all the pre-built dependencies using the following command.

```shell
sudo apt install -y build-essential pkg-config python3 python3-dev \
  python3-pygments flex bison r-base r-base-dev r-cran-ggplot2 r-cran-svglite \
  inkscape texlive-full latexmk libhwloc15 libhwloc-dev cmake openmpi-bin \
  openmpi-common openmpi-doc libopenmpi3 libopenmpi-dev intel-oneapi-mkl \
  intel-oneapi-mkl-devel git
```

The other dependencies must be built manually from sources either because they
are not available through APT our the desired versions are not provided.

We use the folders `$HOME/src` and `$HOME/bin` to store sources and builds,
respectively. We can create these folders with:

```shell
mkdir -p $HOME/src
mkdir -p $HOME/bin
```

Then, we download all the sources into `$HOME/src`.

```shell
cd $HOME/src
wget https://github.com/jemalloc/jemalloc/releases/download/4.5.0/jemalloc-4.5.0.tar.bz2
wget https://files.inria.fr/starpu/starpu-1.3.9/starpu-1.3.9.tar.gz
wget https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz
git clone https://github.com/jeromerobert/hmat-oss
cd hmat-oss
git checkout 6ad59b7db2be69ac3f60e4a58795b71855e1b523
cd $HOME/src
git clone https://gitlab.inria.fr/solverstack/test_fembem
cd test_fembem
git checkout 59d1983493a95ec483b4d931afcc70b687474aa5
cd $HOME/src
git clone https://github.com/xianyi/OpenBLAS.git
cd OpenBLAS
git checkout v0.3.20
```

Follow the instructions to build and install:

1. OpenBLAS,
   ```shell
   cd $HOME/src/OpenBLAS
   make -j$(nproc)
   make PREFIX=$HOME/bin/OpenBLAS/ install
   ```
2. Jemalloc,
   ```shell
   cd $HOME/src
   mkdir jemalloc
   tar -xvf jemalloc-4.5.0.tar.bz2 -C jemalloc --strip-components 1
   cd $HOME/src/jemalloc
   ./configure --prefix=$HOME/bin/jemalloc
   make -j$(nproc) install
   ```
3. StarPU.
   ```shell
   cd $HOME/src
   mkdir -p starpu
   tar -xvzf starpu-1.3.9.tar.gz -C starpu --strip-components 1
   cd $HOME/src/starpu
   ./configure --enable-maxcpus=128 --disable-build-examples \
               --disable-build-doc --prefix=$HOME/bin/starpu
   make -j$(nproc) install
   ```
   
Note that the solvers Chameleon, HMAT-OSS and `test_FEMBEM` rely on the BLAS [8]
and LAPACK [9] linear algebra routines. In this study, we compare the
performance of the solvers built with two different implementations of these
libraries, the open-source OpenBLAS [6] implementation and the vendor-specific
Intel(R) MKL implementation. For the latter, only pre-built binaries are
available.

Below, we provide instructions for building Chameleon, HMAT-OSS and
`test_FEMBEM` with OpenBLAS and Intel(R) MKL. For the builds using OpenBLAS, the
sources will be placed under `$HOME/src/openblas` and the binaries under
`$HOME/bin/openblas`. For the builds using Intel(R) MKL, we will place the
sources under `$HOME/src/mkl` and the binaries under `$HOME/bin/mkl`.

##### Build using OpenBLAS
   
1. Chameleon
   ```shell
   cd $HOME/src
   mkdir -p openblas/chameleon
   tar -xvf chameleon-1.1.0.tar.gz -C openblas/chameleon --strip-components 1
 
   cd $HOME/src/openblas/chameleon
   mkdir build
   cd build
   cmake -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=OpenBLAS \
         -DBUILD_SHARED_LIBS=ON \
         -DCMAKE_PREFIX_PATH="$HOME/bin/OpenBLAS;$HOME/bin/starpu" \
         -DCMAKE_INSTALL_PREFIX=$HOME/bin/openblas/chameleon ..
   make -j$(nproc) install
   ```
2. HMAT-OSS
   ```shell
   cd $HOME/src/
   mkdir -p hmat-oss/build
   cp -ar hmat-oss openblas/hmat-oss
   
   cd $HOME/src/openblas/hmat-oss/build
   cmake -DHMAT_JEMALLOC=ON -DHMAT_EXPORT_BUILD_DATE=ON \
         -DCMAKE_INSTALL_PREFIX=$HOME/bin/openblas/hmat-oss \
         -DCMAKE_PREFIX_PATH="$HOME/bin/jemalloc;$HOME/bin/starpu;$HOME/bin/OpenBLAS" ..
   make -j$(nproc) install
   ```
3. **test_FEMBEM**
   ```shell
   cd $HOME/src/
   mkdir -p test_fembem/build
   cp -ar test_fembem openblas/test_fembem

   cd $HOME/src/openblas/test_fembem/build
   cmake -DCMAKE_INSTALL_PREFIX=$HOME/bin/openblas/test_fembem \
         -DCMAKE_PREFIX_PATH="$HOME/bin/starpu;$HOME/bin/OpenBLAS;$HOME/bin/openblas/chameleon;$HOME/bin/openblas/hmat-oss" \
         ..
   LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HOME/bin/jemalloc/lib:$HOME/bin/starpu/lib:$HOME/bin/openblas/chameleon/lib:$HOME/bin/openblas/hmat-oss/lib" make -j$(nproc) install
   ```

##### Build using Intel(R) MKL

At first, we have to set up the environment variables related to the Intel(R)
software using the provided shell script.

```shell
source /opt/intel/oneapi/setvars.sh
```

We can now proceed with the build.
   
1. Chameleon
   ```shell
   cd $HOME/src
   mkdir -p mkl/chameleon
   tar -xvf chameleon-1.1.0.tar.gz -C mkl/chameleon --strip-components 1
   
   cd $HOME/src/mkl/chameleon
   mkdir build
   cd build
   cmake -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=Intel10_64lp \
         -DBUILD_SHARED_LIBS=ON -DCMAKE_PREFIX_PATH="$HOME/bin/starpu" \
         -DCMAKE_INSTALL_PREFIX=$HOME/bin/mkl/chameleon ..
   make -j$(nproc) install
   ```
2. HMAT-OSS
   ```shell
   cd $HOME/src/
   mkdir -p hmat-oss/build
   mv hmat-oss mkl/hmat-oss
   
   cd $HOME/src/mkl/hmat-oss/build
   cmake -DHMAT_JEMALLOC=ON -DHMAT_EXPORT_BUILD_DATE=ON \
         -DCMAKE_INSTALL_PREFIX=$HOME/bin/mkl/hmat-oss \
         -DCMAKE_PREFIX_PATH="$HOME/bin/jemalloc;$HOME/bin/starpu" ..
   make -j$(nproc) install
   ```
3. **test_FEMBEM**
   ```shell
   cd $HOME/src/
   mkdir -p test_fembem/build
   mv test_fembem mkl/test_fembem
   
   cd $HOME/src/mkl/test_fembem/build
   cmake -DCMAKE_INSTALL_PREFIX=$HOME/bin/mkl/test_fembem \
         -DCMAKE_PREFIX_PATH="$HOME/bin/starpu;$HOME/bin/mkl/chameleon;$HOME/bin/mkl/hmat-oss" \
         ..
   LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBRARY_PATH:$HOME/bin/jemalloc/lib:$HOME/bin/starpu/lib:$HOME/bin/mkl/chameleon/lib:$HOME/bin/mkl/hmat-oss/lib" make -j$(nproc) install
   ```

### Running the experiments

Let us clone this repository.

```shell
git clone \
  https://gitlab.inria.fr/tutoriel-guix-compas-2023/test_fembem/reference-study.git
```

Then, we navigate to the repository.

```shell
cd reference-study
```

At this point, we can launch the benchmark campaing using the dedicated `run.sh`
shell script located in the ~benchmarks~ folder. The parameters for the
different test cases to run are defined in `./benchmarks/definitions.csv`. See
the comments in `./benchmarks/run.sh` for more details about this file.

Note that we will run the benchmarks twice. The first time we will rely on the
`test_FEMBEM`, Chameleon and HMAT-OSS built using OpenBLAS and the second time
we will consider their Intel(R) MKL builds. The results of the first and the
second run will appear in `./benchmarks/results-openblas` and
`./benchmarks/results-mkl`, respectively.

#### Runs with OpenBLAS

To run the benchmarks with the Docker container, use the following command. The
`OpenBLAS.env` file contains a list of environment variables to be set within
the container in order for the `test_FEMBEM`, Chameleon and HMAT-OSS built with
OpenBLAS to run correctly.

```shell
docker run --rm --env-file OpenBLAS.env \
    --mount type=bind,source="$(pwd)",target=/fembem -w /fembem \
    tutoriel-guix-compas-2023/test_fembem/reference-study \
    /bin/bash ./benchmarks/run.sh -d ./benchmarks/definitions.csv \
    -o ./benchmarks/results-openblas
```

If you have built the software environment manually, type these commands.

```shell
bash
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HOME/bin/jemalloc/lib:$HOME/bin/OpenBLAS/lib:$HOME/bin/starpu/lib:$HOME/bin/openblas/chameleon/lib:$HOME/bin/openblas/hmat-oss/lib"
export PATH="$PATH:$HOME/bin/openblas/test_fembem/bin"
./benchmarks/run.sh -d ./benchmarks/definitions.csv -o ./benchmarks/results-openblas
exit
```

#### Runs with Intel(R) MKL

To run the benchmarks with the Docker container, use the following command. The
`MKL.env` file contains a list of environment variables to be set within the
container in order for the `test_FEMBEM`, Chameleon and HMAT-OSS built with
Intel(R) MKL to run correctly.

```shell
docker run --rm --env-file MKL.env \
    --mount type=bind,source="$(pwd)",target=/fembem -w /fembem \
    tutoriel-guix-compas-2023/test_fembem/reference-study \
    /bin/bash ./benchmarks/run.sh -d ./benchmarks/definitions.csv \
    -o ./benchmarks/results-mkl
```

If you have built the software environment manually, type these commands.

```shell
bash
source /opt/intel/oneapi/setvars.sh
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$LIBRARY_PATH:$HOME/bin/jemalloc/lib:$HOME/bin/starpu/lib:$HOME/bin/mkl/chameleon/lib:$HOME/bin/mkl/hmat-oss/lib"
export PATH="$PATH:$HOME/bin/mkl/test_fembem/bin"
./benchmarks/run.sh -d ./benchmarks/definitions.csv -o ./benchmarks/results-mkl
exit
```

### Post-processing

Once the benchmarks have finished running, we can post-processes the results in
`./benchmarks/results-openblas/results.csv` and
`./benchmarks/results-mkl/results.csv` using the `plot.R` R script.

We generate the figures from within the root of the repository either using the
Docker container:

```shell
docker run --rm --mount type=bind,source="$(pwd)",target=/fembem -w /fembem \
    tutoriel-guix-compas-2023/test_fembem/reference-study Rscript plot.R \
    ./benchmarks/results-openblas/results.csv \
    ./benchmarks/results-mkl/results.csv
```
  
or directly from the command line in case of a manual build of the software
environment:

```shell
Rscript plot.R ./benchmarks/results-openblas/results.csv \
    ./benchmarks/results-mkl/results.csv
```

At the end, we produce the PDF document of the study either using the
Docker container:

```shell
docker run --rm --mount type=bind,source="$(pwd)",target=/fembem -w /fembem \
    tutoriel-guix-compas-2023/test_fembem/reference-study \
    latexmk --shell-escape -f -pdf -bibtex -interaction=nonstopmode study
```

or directly from the command line in case of a manual build of the software
environment:

```shell
latexmk --shell-escape -f -pdf -bibtex -interaction=nonstopmode study
```

Et voilà !

## References

1. Docker: Accelerated, Containerized Application Development
   [https://www.docker.com/](https://www.docker.com/).
2. Best practices for writing Dockerfiles - Docker Documentation
   [https://docs.docker.com/develop/develop-images/dockerfile_best-practices/](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/).
3. test_FEMBEM: A simple application for testing dense and sparse solvers with
   pseudo-FEM or pseudo-BEM matrices
   [https://gitlab.inria.fr/solverstack/test_fembem](https://gitlab.inria.fr/solverstack/test_fembem).
4. Chameleon: A dense linear algebra software for heterogeneous architectures
   [https://solverstack.gitlabpages.inria.fr/chameleon/](https://solverstack.gitlabpages.inria.fr/chameleon/).
5. HMAT-OSS: A hierarchical matrix library
   [https://github.com/jeromerobert/hmat-oss](https://github.com/jeromerobert/hmat-oss).
6. OpenBLAS: An optimized BLAS library
   [http://www.openblas.net/](http://www.openblas.net/).
7. Intel(R) Math Kernel Library: Intel(R)-Optimized Math Library for Numerical
   Computing
   [https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html).
8. BLAS (Basic Linear Algebra Subprograms)
   [https://www.netlib.org/blas/](https://www.netlib.org/blas/).
9. LAPACK — Linear Algebra PACKage
   [https://www.netlib.org/lapack/](https://www.netlib.org/lapack/).
10. Intel(R) Core(TM) i5-12500H Processor Product Specification
    [https://ark.intel.com/content/www/us/en/ark/products/96141/intel-core-i512500h-processor-18m-cache-up-to-4-50-ghz.html](https://ark.intel.com/content/www/us/en/ark/products/96141/intel-core-i512500h-processor-18m-cache-up-to-4-50-ghz.html).
