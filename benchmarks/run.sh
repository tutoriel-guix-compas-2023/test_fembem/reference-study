#! /usr/bin/env bash
#
# Run 'test_FEMBEM' benchmarks.
#
# This script runs 'test_FEMBEM' benchmarks for all the parameter combinations
# defined in an input comma-separated values (*.csv) file. The file must feature
# the following columns:
#
#   1. system_type: type of linear system ('fem', 'bem' or 'fembem')
#   2. solver: the solver to use ('hmat' or 'chameleon')
#   3. threads: number of threads to use for computation
#   4. nbpts: number of unknowns in the system (size of the system)
#   5. arith: arithmetic and precision of the coefficients ('s' for simple real,
#      'd' for double real, 'c' for simple complex or 'z' for double complex)
#   6. sym: symmetry of the coefficient matrix ('1' for symmetric or '0' for
#      non-symmetric)
#
# Note that the benchmark definition file must have a header.

# Define a help function that can be triggered using the `-h' option.
function help() {
  echo "Run test_FEMBEM benchmarks defined in FILE." >&2
  echo "Usage: $(basename $0) [options]" >&2
  echo >&2
  echo "Options:" >&2
  echo "  -h               Print this help message." >&2
  echo "  -d FILE          Run the benchmarks defined in FILE. This is a" \
       "mandatory option." >&2
  echo "  -e EXECUTABLE    Set test_FEMBEM executable to EXECUTABLE. The" \
       "default value is 'test_FEMBEM'." >&2
  echo "  -o FOLDER        Run the benchmarks, then store the logs and" \
       " results in FOLDER. The default value is '$(pwd)'." >&2
}

# Parse arguments.

DEFINITIONS=""
TEST_FEMBEM="test_FEMBEM"
OUTPUT="$(pwd)"

while getopts ":hd:e:o:" option;
do
  case $option in
    d)
      DEFINITIONS=$OPTARG

      if test ! -f $DEFINITIONS;
      then
        echo "Error: '$DEFINITIONS' is not a valid file!" >&2
        exit 1
      fi
      ;;
    e)
      TEST_FEMBEM=$OPTARG

      if test ! -x $TEST_FEMBEM;
      then
        echo "Error: '$TEST_FEMBEM' is not a valid executable!" >&2
        exit 1
      fi
      ;;
    h)
      help
      exit 0
      ;;
    o)
      OUTPUT=$OPTARG

      if test ! -d $OUTPUT;
      then
        echo "Error: '$OUTPUT' is not a valid folder!" >&2
        exit 1
      fi
      ;;
    \?) # Unknown option
      echo "Arguments mismatch! Invalid option '-$OPTARG'." >&2
      echo
      help
      exit 1
      ;;
    :) # Missing option argument
      echo "Arguments mismatch! Option '-$OPTARG' expects an argument!" >&2
      echo
      help
      exit 1
      ;;
    *)
      help
      exit 1
      ;;
  esac
done

# Check if the benchmark definition file was specified.
if test "$DEFINITIONS" == "";
then
  echo "Error: No benchmark definition file specified!" >&2
  exit 1
fi

# Get the number of line in the definition file.
DEFINITIONS_NBLINES=$(wc -l $DEFINITIONS | cut -d ' ' -f 1)

# Check if the file ends with a newline and if so, fix the number of lines.
if test $(tail -c 1 $DEFINITIONS | wc -l) -gt 0;
then
  DEFINITIONS_NBLINES=$(expr $DEFINITIONS_NBLINES - 1)
fi

# Read the definition file without the header.
DEFINITIONS_WITHOUT_HEADER=$(cat $DEFINITIONS | tail -n $DEFINITIONS_NBLINES)

# Extract header from the definition file. The script will reuse it to create a
# results file.
HEADER=$(cat $DEFINITIONS | head -n 1)

# Keep the current working directory.
CWD=$(pwd)

# Create the output folder, if necessary, and navigate to it.
mkdir -p $OUTPUT
cd $OUTPUT

# Define the name of the results file.
RESULTS="results.csv"

# Add header to the results file.
echo "$HEADER,tps_facto,tps_solve,error" > $RESULTS

# Initialize a counter for failed benchmarks.
FAILURES=0

# Iterate over the definitions and run the benchmarks.
for line in $DEFINITIONS_WITHOUT_HEADER;
do
  # Extract benchmark parameters from the current line.
  SYSTEM_TYPE=$(echo $line | cut -d ',' -f 1)
  SOLVER=$(echo $line | cut -d ',' -f 2)
  THREADS=$(echo $line | cut -d ',' -f 3)
  NBPTS=$(echo $line | cut -d ',' -f 4)
  ARITH=$(echo $line | cut -d ',' -f 5)
  SYM=$(echo $line | cut -d ',' -f 6)

  # Choose benchmark log file name based on the above values.
  LOG="$SYSTEM_TYPE-$SOLVER-$THREADS-$NBPTS-$ARITH-$SYM.log"

  # Keep a benchmark name for later.
  BENCHMARK="$SYSTEM_TYPE, $SOLVER (threads: $THREADS, unknowns: $NBPTS,"
  BENCHMARK="$BENCHMARK arithmetic: $ARITH, symmetric: $SYM)"

  # Transform the parameters to 'test_FEMBEM' options.
  SYSTEM_TYPE="--$SYSTEM_TYPE"
  SOLVER="-solve$SOLVER"

  if test "$SOLVER" == "hmat";
  then
    SOLVER="--hmat --hmat-eps-assemb 1e-3 --hmat-eps-recompr 1e-3 $SOLVER"
  fi

  NBPTS="-nbpts $NBPTS"

  ARITH="-$ARITH"

  if test $SYM -eq 1;
  then
    SYM="--sym"
  else
    SYM="--nosym"
  fi

  # Run 'test_FEMBEM' with the given parameters.
  echo -n "Running $BENCHMARK..."
  OMP_NUM_THREADS=1 MKL_NUM_THREADS=1 STARPU_NCPU=$THREADS $TEST_FEMBEM \
  $SYSTEM_TYPE $SYM $ARITH $NBPTS $SOLVER > $LOG 2>&1
  if test $? -eq 0;
  then
    # On success, parse computation times and relative error from the log file.
    TPS_FACTO=$(cat $LOG | grep -E "<PERFTESTS> TpsCpu.*Facto" | \
                  cut -d '=' -f 2 | sed 's/[^0-9.]//g')
    TPS_SOLVE=$(cat $LOG | grep -E "<PERFTESTS> TpsCpuSolve" | \
                  cut -d '=' -f 2 | sed 's/[^0-9.]//g')
    ERROR=$(cat $LOG | grep -E "<PERFTESTS> Error" | \
              cut -d '=' -f 2 | sed 's/[^-0-9e.+]//g')

    # Add the parsed values to the results file.
    echo "$line,$TPS_FACTO,$TPS_SOLVE,$ERROR" >> $RESULTS

    # Show success message.
    echo "Done"
  else
    # Show failure message and increase the failed benchmark counter.
    echo "Failed"
    FAILURES=$(expr $FAILURES + 1)
  fi
done

# Show global results.
echo
echo "Successful benchmarks: $(expr $DEFINITIONS_NBLINES - $FAILURES)"
echo "Failed benchmarks: $FAILURES"

# Perform cleaning.
rm -f testHMAT_matrix.json

# Restore the initial working directory and exit.
cd $CWD
exit 0
